*** Settings ***
Library    CamundaLibrary   ${CAMUNDA_HOST}

*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080

*** Tasks ***
Upload model from group 0 exercise
    Deploy model from file    bpmn/group_exercise_0.bpmn

Upload model from exercise 3
    Deploy model from file    bpmn/exercise_3.bpmn

Upload model from exercise 6
    Deploy model from file    bpmn/exercise_6.bpmn

Upload model from exercise 8
    Deploy model from file    bpmn/exercise_8.bpmn

Upload model from exercise 9
    Deploy model from file    bpmn/exercise_9.bpmn

Upload model from exercise 10
    Deploy model from file    bpmn/exercise_10.bpmn