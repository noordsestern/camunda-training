*** Settings ***
Documentation    *Add Process Variables*
...
...   *Goal*
...
...   In this lab you will use the fetchAndLock API call to read the content from a process variable. Additionally we will implement an Exclusive Data-based Gateway (decision) to the process.
...
...   *Short description*
...
...   - Add the amount that has to be paid at the process instance start.
...   - Use the amount value in first worker to check it against the credit of the customer and test your changes.
...   - Add the creditSufficient result at service completion and test your changes. Use the REST API to start process instances.
...   - Adjust the second service to charge the remaining amount.

Library    CamundaLibrary   ${CAMUNDA_HOST}
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_3.bpmn    PaymentProcess
Test Teardown    Sleep    0.5


*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080


*** Tasks ***
Start Process
    ${random_amount}    Get random number between 0 and 100
    ${variables}    Create Dictionary    amount=${random_amount}
    start process    PaymentProcess    variables=${variables}


Charge credit
    Fetch and lock from 'charge_credit' topic
    ${return_values}    Create Dictionary    creditSufficient=${{50 > ${WORKLOAD}[amount]}}
    complete task    ${return_values}


Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    ${return_values}    Create Dictionary    credit_card_chared=${True}
    complete task    ${return_values}


*** Keywords ***
Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    ${variables}    Fetch Workload    ${topic}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance     No workload fetched
    Set test variable    ${WORKLOAD}    ${variables}


Get random number between ${min} and ${max}
    ${random_amount}    Evaluate    random.randint(${min},${max})    modules=random
    [Return]    ${random_amount}
