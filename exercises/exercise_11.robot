*** Settings ***
Documentation    *Test the Payment Process with a Postman Collection*
...
...   *Goal*
...
...   In this lab you should test the happy path of the payment process with a Postman Collection.
...
...   *Short description*
...
...   - Import the prepared Postman collection into your Postman.
...   - Select your process model in the deploy request.
...   - Adjust the body and tests of each step to fit your process model.
...   - Run the collection.

Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    RequestsLibrary
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_8.bpmn    PaymentProcess


*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080
${WAIT_MILLISECONDS_FOR_WORKLOAD}    1000


*** Tasks ***    #actually test cases, but *** Test Cases *** messes with mixed executions in pipeline
Test Payments below 50 are not charged on creditcard
    [Template]    Payments below 50 should not be charged on credit card
    0
    5
    10
    20
    30
    40
    49

Test Payments of 50 and more are charged on creditcard
    [Template]    Payments of 50 and more should be charged on creditcard
    50
    51
    60
    1000

*** Keywords ***

# TEST TEMPLATES +++++++++++++++++++++++++++++
Payments below 50 should not be charged on credit card
    [Arguments]    ${amount}
    Given Request payment of '${amount}'
    When Charge credit
    Then Credit Card is not charged
    [Teardown]    Delete all process instances    ${{['PaymentProcess']}}

Payments of 50 and more should be charged on creditcard
    [Arguments]    ${amount}
    Given Request payment of '${amount}'
    When Charge credit
    Then Credit Card is charged
    [Teardown]    Delete all process instances    ${{['PaymentProcess']}}


# ASSERTIONS +++++++++++++++++++++++++++++
Credit Card is not charged
    ${variables}    Fetch Workload    charge_credit_card    async_response_timeout=${WAIT_MILLISECONDS_FOR_WORKLOAD}

    Should Be Empty    ${variables}    Credit card was not supposed to be charged.

Credit Card is charged
    ${variables}    Fetch Workload    charge_credit_card    async_response_timeout=${WAIT_MILLISECONDS_FOR_WORKLOAD}

    Should Not Be Empty    ${variables}    Credit Card should have been charged
    Should Not be Empty    '${variables}[amount]'    Credit card
    ${is_greater_50}    Evaluate    ${variables}[amount] >= 50
    Should be True    ${variables}[amount] >= 50

# TEST KEYWORDS +++++++++++++++++++++++++++++
Request payment of '${amount}'
    ${process_variables}    Create Dictionary    amount=${{{ "value":${amount} }}}    troublemaker=${{{"value": ${False}}}}

    ${request}    Create dictionary
    ...    messageName=msg_payment_requested
    ...    processVariables=${process_variables}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    Create Session    training    ${CAMUNDA_HOST}
    ${response}    POST on session    training    engine-rest/message    json=${request}    headers=${headers}    expected_status=204

Charge credit
    Fetch and lock from 'charge_credit' topic
    ${return_values}    Create Dictionary    creditSufficient=${{50 > ${WORKLOAD}[amount]}}
    complete task    ${return_values}

Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    Run keyword if    ${WORKLOAD}[troublemaker]    throw error
    ${return_values}    Create Dictionary    credit_card_chared=${True}
    complete task    ${return_values}

Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    ${variables}    Fetch Workload    ${topic}    async_response_timeout=${WAIT_MILLISECONDS_FOR_WORKLOAD}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance     No workload fetched
    Set test variable    ${WORKLOAD}    ${variables}