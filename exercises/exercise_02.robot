*** Settings ***
Library    CamundaLibrary    ${CAMUNDA_HOST}
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    group_exercise_0.bpmn    PaymentProcess

*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080

*** Tasks ***
Run exercise 2
    ${variables}    Create Dictionary    creditSufficient=${False}
    ${response}    start process   PaymentProcess    ${variables}
    log    ${response}