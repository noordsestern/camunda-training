*** Settings ***
Documentation    *Add an Order Process to call Payment Process*
...
...   *Goal*
...
...   Create an Order Process and deploy it to the engine. The order process invokes the payment process created in the earlier exercises and waits until the payment is completed before it continues.
...
...   *Short description*
...
...   - Create a new BPMN diagram in the modeler.
...   - The process is started when a new order is placed. It calculates a random orderId and saves it as business key, it sends a message to start the payment process and waits until the payment is completed, fetches the ordered goods and ships them.
...   - Model the start payment service as an external service.
...   - Change the end event of the payment process as a message end event.
...   - Implement the send tasks and events as external tasks.
...   - Add new topics for the new tasks in the worker code. Use POST /message to send messages between the processes.
...   - Deploy the processes and test them with a rest client.
...   - Check the progress in the cockpit.

Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    RequestsLibrary
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_10.bpmn    PaymentProcess    OrderProcess
Test Teardown    Sleep    0.5



*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080


*** Tasks ***
Start Process
    [Documentation]    Need to use RequestsLibrary, because CamundaLibrary does not support businesskey, yet
    Create Session    training    ${CAMUNDA_HOST}
    ${order_number}    Get random number between 1000 and 9999

    ${request}    Create dictionary
    ...    businessKey=Order ${order_number}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/process-definition/key/OrderProcess/start    json=${request}    headers=${headers}    expected_status=200

Start payment
    [Documentation]    BusinessKey is added as order_id for later correlation
    Fetch and lock from 'start_payment' topic
    ${random_amount}    Get random number between 10 and 30
    ${fetch_response}    Get fetch response
    ${process_variables}    Create Dictionary    amount=${{{ "value":${random_amount} }}}    troublemaker=${{{"value": ${False}}}}    order_id=${{ {"value": "${fetch_response}[business_key]"} }}
    send message    msg_payment_requested    ${process_variables}
    complete task

Charge credit
    Fetch and lock from 'charge_credit' topic
    ${return_values}    Create Dictionary    creditSufficient=${{50 > ${WORKLOAD}[amount]}}
    complete task    ${return_values}


Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    Run keyword if    ${WORKLOAD}[troublemaker]    throw error    ${{ {'do_manually': {'value': True}} }}
    complete task

Deduct credit
    Fetch and lock from 'deduct_credit' topic
    complete task

Send message 'payment received'
    Fetch and lock from 'payment_received' topic
    Send correlating message    msg_payment_received    ${WORKLOAD}[order_id]
    complete task


*** Keywords ***
Throw error
    [Arguments]    ${variables}
    Create Session    training    ${CAMUNDA_HOST}
    ${fetch_response}    get fetch response
    ${worker_id}    Set Variable    ${fetch_response}[worker_id]

    ${request}    Create dictionary
    ...    workerId=${fetch_response}[worker_id]
    ...    errorMessage=Troublemaker
    ...    errorCode=eccc
    ...    variables=${variables}


    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/external-task/${fetch_response}[id]/bpmnError    json=${request}    headers=${headers}    expected_status=204
    drop fetch response
    Pass Execution    An error occured


Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    ${variables}    Fetch workload    ${topic}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance    No workload fetched
    Set test variable    ${WORKLOAD}    ${variables}


Get random number between ${min} and ${max}
    ${random_amount}    Evaluate    random.randint(${min},${max})    modules=random
    [Return]    ${random_amount}

Send message
    [Documentation]    Sending a message with process variables
    [Arguments]    ${msg_name}    ${process_variables}
    Create Session    training    ${CAMUNDA_HOST}
    ${request}    Create dictionary
    ...    messageName=${msg_name}
    ...    processVariables=${process_variables}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/message    json=${request}    headers=${headers}    expected_status=204

Send correlating message
    [Documentation]    Sending a correlation method correlating on businessKey
    [Arguments]    ${msg_name}    ${business_key}
    Create Session    training    ${CAMUNDA_HOST}
    ${request}    Create dictionary
    ...    messageName=${msg_name}
    ...    businessKey=${business_key}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/message    json=${request}    headers=${headers}    expected_status=204
