*** Settings ***
Documentation    *Add Incident Handling*
...
...   *Goal*
...
...   Handle exceptions in the first Check Credit external worker and inform the engine about them. Respond with either POST /external-task/failure or use code within the external worker to fail a task and to retry it.
...
...   *Short description*
...
...   - Change the implementation of either or both of the workers to catch exceptions.
...   - In the case of an exception, send a POST external-task/failure with error message and error details in the body or use code in the external worker.
...   - Don’t do any retries to see the incident in the cockpit.
...   - Start a process instance that causes the exception in the worker.
...   - Inspect the incident in the cockpit and solve it manually in the cockpit.

Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    RequestsLibrary
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_6.bpmn    PaymentProcess
Test Teardown    Sleep    0.5



*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080


*** Tasks ***
Start Process
    [Setup]    Create Session    training    ${CAMUNDA_HOST}
    ${random_amount}    Get random number between 0 and 100
    ${process_variables}    Create Dictionary    amount=${{{ "value":${random_amount} }}}    troublemaker=${{{"value": ${True}}}}

    ${request}    Create dictionary
    ...    messageName=msg_payment_requested
    ...    processVariables=${process_variables}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/message    json=${request}    headers=${headers}    expected_status=204


Charge credit
    Fetch and lock from 'charge_credit' topic
    Run keyword if    ${WORKLOAD}[troublemaker]    throw failure
    ${return_values}    Create Dictionary    creditSufficient=${{50 > ${WORKLOAD}[amount]}}
    complete task    ${return_values}


Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    ${return_values}    Create Dictionary    credit_card_chared=${True}
    complete task    ${return_values}


*** Keywords ***
Throw Failure
    Create Session    training    ${CAMUNDA_HOST}
    ${fetch_response}    get fetch response
    ${worker_id}    Set Variable    ${fetch_response}[worker_id]

    ${request}    Create dictionary
    ...    workerId=${fetch_response}[worker_id]
    ...    errorMessage=Troublemaker
    ...    errorDetails=We don't serve trouble
    ...    retries=0


    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/external-task/${fetch_response}[id]/failure    json=${request}    headers=${headers}    expected_status=204


Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    ${variables}    Fetch Workload    ${topic}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance     No workload fetched
    Set test variable    ${WORKLOAD}    ${variables}


Get random number between ${min} and ${max}
    ${random_amount}    Evaluate    random.randint(${min},${max})    modules=random
    [Return]    ${random_amount}
