*** Settings ***
Documentation    *Implement an External Worker*
...
...   *Goal*
...
...   The goal of this lab is to implement this external task worker in a programming language of your choice.
...
...   *Short description*
...
...   - Implement a worker using the external task client for Javascript or C# or Python
...   - start a process instance with a rest client.
...   - Follow the progress in the cockpit.

Library    CamundaLibrary   ${CAMUNDA_HOST}
Test Teardown    Sleep    0.5
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_3.bpmn    PaymentProcess

*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080

*** Tasks ***
Start Process
    ${variables}    Create Dictionary    creditSufficient=${False}
    start process    PaymentProcess    variables=${variables}

Charge credit
    Fetch and lock from 'charge_credit' topic
    complete task


Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    complete task

*** Keywords ***
Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    Fetch Workload    ${topic}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance     No workload fetched