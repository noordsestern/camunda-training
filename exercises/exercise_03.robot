*** Settings ***
Documentation    *Model External Service Tasks and Run it With a REST Client*
...
...   *Goal*
...
...   The goal of this lab is to model the service tasks as external services and run the process manually with a REST client.
...
...   *Short description*
...
...   - Model the tasks as external service tasks.
...   - Start a process instance with the REST client.
...   - Follow the progress in the cockpit.
...   - Fetch and complete the external tasks with the REST client.

Library    Collections
Library    RequestsLibrary
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_3.bpmn    PaymentProcess

*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080

*** Tasks ***
Start Process
    No Operation

Charge credit
    ${response}    Fetch and lock charge_credit
    Pass execution if    not ${response}    Nothing to process
    complete ${response}[id]


Charge credit card
    ${response}    Fetch and lock charge_credit_card
    Pass execution if    not ${response}    Nothing to process
    complete ${response}[id]


*** Keywords ***
Fetch and lock ${topicName}
    Create Session    training    ${CAMUNDA_HOST}
    ${topic}    Create Dictionary
    ...    topicName=${topicName}
    ...    lockDuration=1000

    ${topics}    Create List    ${topic}

    ${request}    Create dictionary
    ...    topics=${topics}
    ...    workerId=robot
    ...    maxTasks=1

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/external-task/fetchAndLock    json=${request}    headers=${headers}    expected_status=200

    [Return]    ${response.json()}

Complete ${id}
    Create Session    training    ${CAMUNDA_HOST}
    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${request}    Create dictionary
    ...    workerId=robot

    ${response}    POST on session    training    engine-rest/external-task/${id}/complete    json=${request}    headers=${headers}    expected_status=200
