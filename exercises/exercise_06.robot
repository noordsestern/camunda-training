*** Settings ***
Documentation    *Start the Payment From a Message*
...
...   *Goal*
...
...   To be prepared for message correlation you should start the process with a message.
...
...   *Short description*
...
...   - Morph the None start event to a Message start event.
...   - Start the process with submitting a message to REST api. The payload should include the amount to pay.
...   - Test the process and check the flow in cockpit.

Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    RequestsLibrary
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_6.bpmn    PaymentProcess
Test Teardown    Sleep    0.5



*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080


*** Tasks ***
Start Process
    Create Session    training    ${CAMUNDA_HOST}
    ${random_amount}    Get random number between 0 and 100
    ${process_variables}    Create Dictionary    amount=${{{ "value":${random_amount} }}}

    ${request}    Create dictionary
    ...    messageName=msg_payment_requested
    ...    processVariables=${process_variables}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/message    json=${request}    headers=${headers}    expected_status=204


Charge credit
    Fetch and lock from 'charge_credit' topic
    ${return_values}    Create Dictionary    creditSufficient=${{50 > ${WORKLOAD}[amount]}}
    complete task    ${return_values}


Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    ${return_values}    Create Dictionary    credit_card_chared=${True}
    complete task    ${return_values}


*** Keywords ***
Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    ${variables}    Fetch Workload    ${topic}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance     No workload fetched
    Set test variable    ${WORKLOAD}    ${variables}


Get random number between ${min} and ${max}
    ${random_amount}    Evaluate    random.randint(${min},${max})    modules=random
    [Return]    ${random_amount}
