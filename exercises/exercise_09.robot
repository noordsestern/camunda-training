*** Settings ***
Documentation    *Add Task Forms and Use the Camunda Tasklist*
...
...   *Goal*
...
...   In this lab you add a simple task form to your process and deploy the project to the Camunda BPM platform.
...
...   *Short description*
...
...   - Add a user task for error handling if the business error is resolvable.
...   - Fill the candidate group as “accounting”.
...   - Create an html file for the form and adjust it to your process.
...   - Bind the form in the form key to the user task.
...   - Deploy the form and the process in one deployment as in exercise 2.
...   - Start a process instance and check the task form in the task list.

Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    RequestsLibrary
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_9.bpmn    PaymentProcess
Test Teardown    Sleep    0.5



*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080


*** Tasks ***
Start Process
    [Tags]    manual
    [Setup]    Create Session    training    ${CAMUNDA_HOST}
    ${random_amount}    Get random number between 51 and 100
    ${process_variables}    Create Dictionary    amount=${{{ "value":${random_amount} }}}    troublemaker=${{{"value": ${True}}}}

    ${request}    Create dictionary
    ...    messageName=msg_payment_requested
    ...    processVariables=${process_variables}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/message    json=${request}    headers=${headers}    expected_status=204


Charge credit
    Fetch and lock from 'charge_credit' topic
    ${return_values}    Create Dictionary    creditSufficient=${{50 > ${WORKLOAD}[amount]}}
    complete task    ${return_values}


Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    Run keyword if    ${WORKLOAD}[troublemaker]    throw error    ${{ {'do_manually': {'value': True}} }}
    complete task

Deduct credit
    Fetch and lock from 'deduct_credit' topic
    complete task


*** Keywords ***
Throw error
    [Arguments]    ${variables}
    Create Session    training    ${CAMUNDA_HOST}
    ${fetch_response}    get fetch response
    ${worker_id}    Set Variable    ${fetch_response}[worker_id]

    ${request}    Create dictionary
    ...    workerId=${fetch_response}[worker_id]
    ...    errorMessage=Troublemaker
    ...    errorCode=eccc
    ...    variables=${variables}


    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/external-task/${fetch_response}[id]/bpmnError    json=${request}    headers=${headers}    expected_status=204
    drop fetch response
    Pass Execution    An error occured


Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    ${variables}    Fetch workload    ${topic}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance    No workload fetched
    Set test variable    ${WORKLOAD}    ${variables}


Get random number between ${min} and ${max}
    ${random_amount}    Evaluate    random.randint(${min},${max})    modules=random
    [Return]    ${random_amount}
