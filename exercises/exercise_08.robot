*** Settings ***
Documentation    *Add BPMN Error and Compensation*
...
...   *Goal*
...
...   In this lab we want to improve the error handling. We catch business errors and invoke compensation events and compensation activity.
...
...   *Short description*
...
...   - Attach an error boundary event to the credit card charging activity.
...   - If the error occurs, it should invoke a compensation throw event before the payment is denied.
...   - The deducted credit should be added back again.

Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    RequestsLibrary
Resource    cleanup_camunda.resource
Suite Setup    Prepare camunda    exercise_8.bpmn    PaymentProcess
Test Teardown    Sleep    0.5


*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080


*** Tasks ***
Start Process
    [Setup]    Create Session    training    ${CAMUNDA_HOST}
    ${random_amount}    Get random number between 51 and 100
    ${process_variables}    Create Dictionary    amount=${{{ "value":${random_amount} }}}    troublemaker=${{{"value": ${True}}}}

    ${request}    Create dictionary
    ...    messageName=msg_payment_requested
    ...    processVariables=${process_variables}

    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/message    json=${request}    headers=${headers}    expected_status=204


Charge credit
    Fetch and lock from 'charge_credit' topic
    ${return_values}    Create Dictionary    creditSufficient=${{50 > ${WORKLOAD}[amount]}}
    complete task    ${return_values}


Charge credit card
    Fetch and lock from 'charge_credit_card' topic
    Run keyword if    ${WORKLOAD}[troublemaker]    throw error
    ${return_values}    Create Dictionary    credit_card_chared=${True}
    complete task    ${return_values}

Deduct credit
    Fetch and lock from 'deduct_credit' topic
    complete task


*** Keywords ***
Throw error
    Create Session    training    ${CAMUNDA_HOST}
    ${fetch_response}    get fetch response
    ${worker_id}    Set Variable    ${fetch_response}[worker_id]

    ${request}    Create dictionary
    ...    workerId=${fetch_response}[worker_id]
    ...    errorMessage=Troublemaker
    ...    errorCode=eccc


    ${headers}    Create Dictionary
    ...    Content-Type=application/json

    ${response}    POST on session    training    engine-rest/external-task/${fetch_response}[id]/bpmnError    json=${request}    headers=${headers}    expected_status=204
    Pass Execution    An error occured


Fetch and lock from '${topic}' topic
    [Documentation]    Check whether a process instance has been fetched recently. If not, abort execution of the
    ...                running task.
    ${variables}    Fetch Workload    ${topic}
    ${process_instance}    get fetch response
    Pass execution if    not $process_instance     No workload fetched
    Set test variable    ${WORKLOAD}    ${variables}


Get random number between ${min} and ${max}
    ${random_amount}    Evaluate    random.randint(${min},${max})    modules=random
    [Return]    ${random_amount}
